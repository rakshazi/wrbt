package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"

	bolt "go.etcd.io/bbolt"
)

// Exported boss info
type Export struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Raid       string `json:"raid"`
	Difficulty int    `json:"difficulty"`
	Kills      int    `json:"kills"`
	Wipes      int    `json:"wipes"`
}

// Export data to json file
func runExport() {
	log.Println("Start exporting data to", exportpath, "...")
	data := []*Export{}
	err := db.View(func(tx *bolt.Tx) error {
		tx.Bucket([]byte("metadata")).ForEach(func(k, v []byte) error {
			id := string(k)
			meta := &Meta{}
			err := json.Unmarshal(v, meta)
			if err != nil {
				log.Println("Cannot retreive metadata for boss", id, err)
				return err
			}
			cursor := tx.Bucket([]byte("bosses")).Cursor()
			for bossId, bossValue := cursor.Seek([]byte(id)); bossId != nil && bytes.HasPrefix(bossId, []byte(id)); bossId, bossValue = cursor.Next() {
				boss := &Boss{}
				err := json.Unmarshal(bossValue, boss)
				if err != nil {
					log.Println("Cannot retrieve boss statistics for", id, err)
					return err
				}
				item := &Export{
					Id:         meta.Id,
					Difficulty: boss.Difficulty,
					Name:       meta.Name,
					Raid:       meta.ZoneName,
					Kills:      boss.Kills,
					Wipes:      boss.Wipes,
				}
				data = append(data, item)
			}

			return nil
		})
		return nil
	})
	if err != nil {
		log.Fatal("Cannot export data", err)
	}
	jsonBytes, err := json.Marshal(data)
	if err != nil {
		log.Fatal("Cannot marshal exported data", err)
	}
	err = ioutil.WriteFile(exportpath, jsonBytes, 0644)
	if err != nil {
		log.Fatal("Cannot write exported data to file", err)
	}
	log.Println("done")
}
