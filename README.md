# Warcraft Raid Boss Top [![GoDoc](https://godoc.org/gitlab.com/rakshazi/wrbt?status.svg)](https://godoc.org/gitlab.com/rakshazi/wrbt)

That tool grabs data from multiple sources to produce list of WoW bosses with count of kills/wipes in each raid difficulty


<!-- vim-markdown-toc GitLab -->

* [Algorithm](#algorithm)
* [Usage](#usage)
    - [Usefull tools](#usefull-tools)
    - [Build](#build)
    - [Commands](#commands)
    - [run](#run)
        + [run full flow](#run-full-flow)
        + [run only export](#run-only-export)
    - [Optmimize db (optional)](#optmimize-db-optional)

<!-- vim-markdown-toc -->

## Algorithm

1. Grabs guilds from [Raider.IO](https://raider.io) raid progression API _Special thanks to Ludicrous Speed! Guys, you did awesome service!_
2. Grabs reports list for each guild from [Warcraft Logs](https://www.warcraftlogs.com/) API _Special thanks to Kihra! Without you a lot of things in WoW would be much worse!_
3. Parses each report from list (Warcraft Logs API) and gets boss kills/wipes (global counters for each boss/difficulty)
4. Grabs boss info (npc id, name, raid name) from Warcraft Logs API.
5. Export merged data to json

## Usage

### Usefull tools

* [BBolt cli](https://github.com/etcd-io/bbolt#installing) - to optimize db file

### Build

```bash
CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -v -a -installsuffix cgo -o wrbt
```

### Commands

### run

```bash
wrbt --apikey APIKEY --db BBOLT_DB_FILE --workers WORKERS_COUNT --export TARGET_JSON --only WHAT_TO_DO
```

* **APIKEY** _required_ - Warcraft Logs API Key, you can get it in your profile settings on Warcraft Logs
* **BBOLT_DB_FILE** - Main database file path, default: `./bolt.db`
* **WORKERS_COUNT** - Amount of parallel workers, default: `5`
* **TARGET_JSON** - Exported data json file path, default: `./export.json`
* **WHAT_TO_DO** - If you want to do only one thing (eg: export data), you can set it to that flag, default: `all`

> help available in cli: `wrbt -h`

#### run full flow

```bash
wrbt --apikey APIKEY
```

#### run only export

```bash
wrbt --apikey APIKEY --only export
```

### Optmimize db (optional)

> **NOTE**: requires bbolt cli

```bash
bbolt compact -o new.db bolt.db
mv new.db bolt.db
```
