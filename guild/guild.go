package guild

import (
	"log"
	"sync"

	rio "gitlab.com/rakshazi/wrbt/raiderio"
)

var raidMatrix = make(chan []string)
var results chan *rio.Guild

// Get Guilds from Raider.IO ratings
func GetGuilds(workers int, resultsChan chan *rio.Guild) {
	log.Println("Start collecting guilds...")
	results = resultsChan
	go setRaidMatrix()
	runWorkersPool(workers)

	log.Println("done")
}

// Fill raid matrix with values from Raider.IO package
func setRaidMatrix() {
	for _, raid := range rio.Raids {
		for _, difficulty := range rio.Difficulties {
			for _, region := range rio.Regions {
				raidMatrix <- []string{raid, difficulty, region}
			}
		}
	}
	close(raidMatrix)
}

// Spawns workers
func runWorkersPool(count int) {
	var wg sync.WaitGroup
	for i := 0; i < count; i++ {
		wg.Add(1)
		go worker(&wg)
	}
	wg.Wait()
	close(results)
}

// Worker will load guilds from Raider.IO API with raid matrix values
func worker(wg *sync.WaitGroup) {
	for raid := range raidMatrix {
		guilds, err := rio.GetGuilds(raid[0], raid[1], raid[2])
		if err != nil {
			log.Println("Error getting data for", raid[0], raid[1], raid[2], err)
			continue
		}
		for _, guild := range guilds {
			results <- guild
		}
	}
	wg.Done()
}
