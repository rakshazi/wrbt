package main

import (
	"encoding/json"
	"fmt"
	"log"

	guild "gitlab.com/rakshazi/wrbt/guild"
	rio "gitlab.com/rakshazi/wrbt/raiderio"
)

// Work with guilds
func runGuilds() {
	var guilds = make(chan *rio.Guild)
	var done = make(chan bool)
	go saveGuilds(guilds, done)
	guild.GetGuilds(workers, guilds)
}

// Save guilds to db
func saveGuilds(guilds chan *rio.Guild, done chan bool) {
	var id string    // Guild Key
	var value []byte // Guild object value
	var count int    //Count of new guilds
	var total int    // Total count of guilds
	tx, err := db.Begin(true)
	if err != nil {
		log.Println("Cannot start transaction to save guilds")
		done <- true
	}
	defer tx.Rollback()
	bucket := tx.Bucket([]byte("guilds"))
	for guild := range guilds {
		id = fmt.Sprintf("%s|%s|%s", guild.Name, guild.Realm, guild.Region)
		item := bucket.Get([]byte(id))
		if item != nil {
			continue
		}
		count++
		value, err = json.Marshal(guild)
		if err != nil {
			log.Println("Cannot marshal guild object into bytes", err)
			continue
		}
		if err := bucket.Put([]byte(id), value); err != nil {
			log.Println("Cannot save guild", id, err)
		}
	}
	bucket.ForEach(func(k, v []byte) error {
		total++
		return nil
	})
	if err := tx.Commit(); err != nil {
		log.Println("Cannot commit transaction with guilds")
		done <- true
	}
	log.Println("Guilds saved. Overall:", total, ". New:", count)
	done <- true
}
