# WarcraftLogs API Go [![GoDoc](https://godoc.org/gitlab.com/rakshazi/warcraftlogs-api?status.svg)](https://godoc.org/gitlab.com/rakshazi/warcraftlogs-api)

> fork of [alexejk/go-warcraftlogs](https://github.com/alexejk/go-warcraftlogs)

A Simple golang wrapper for WarcraftLogs API

```golang
import "gitlab.com/rakshazi/wacraftlogs-api"


// Use WarcraftLogs API token (warcraftlogs.com -> account settings -> scroll to the end of page)
api := warcraftlogs.New("cb63bb62fbadb166657d20927a2335ae")
// reports := api.ReportsForGuild("GuildName", "realm-slug-from-blizzard-api", "region")
reports := api.ReportsForGuild("Совет Тирисфаля", "ревущии-фьорд", "EU")

for _, report := range reports {
  // Use report information
}
```


## Implemented

### Reports

* Guild reports list
* Reports list by URL

### Fights

* Fights (pulls) list
* Detailed information about each fight (pull)
* Information about raiders
