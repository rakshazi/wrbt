package main

import (
	"encoding/json"
	"log"

	rio "gitlab.com/rakshazi/wrbt/raiderio"
	wl "gitlab.com/rakshazi/wrbt/warcraftlogs"
	bolt "go.etcd.io/bbolt"
)

// Work with reports queue
func runReports() {
	var reports = make(chan string)
	var done = make(chan bool)
	go saveReportsQueue(reports, done)
	guilds := getGuilds()
	wl.GetReports(workers, apikey, guilds, reports)
}

// Get guilds from db
func getGuilds() []*rio.Guild {
	var guilds []*rio.Guild
	db.View(func(tx *bolt.Tx) error {
		queueBucket := tx.Bucket([]byte("guilds"))

		queueBucket.ForEach(func(k, v []byte) error {
			guild := &rio.Guild{}
			err := json.Unmarshal(v, guild)
			if err != nil {
				return err
			}
			guilds = append(guilds, guild)
			return nil
		})
		return nil
	})

	return guilds
}

// Add reports to the queue
func saveReportsQueue(reports chan string, done chan bool) {
	var total int //Total items in queue
	var count int //New items in queue
	tx, err := db.Begin(true)
	if err != nil {
		log.Println("Cannot start transaction to save reports queue")
		done <- true
	}
	defer tx.Rollback()
	queueBucket := tx.Bucket([]byte("reportsQueue"))
	bucket := tx.Bucket([]byte("reports"))
	for report := range reports {
		queueItem := queueBucket.Get([]byte(report))
		item := bucket.Get([]byte(report))
		if queueItem != nil || item != nil {
			continue
		}
		count++
		if err := queueBucket.Put([]byte(report), nil); err != nil {
			log.Println("Cannot save report to queue", report, err)
		}
	}
	queueBucket.ForEach(func(k, v []byte) error {
		total++
		return nil
	})
	if err := tx.Commit(); err != nil {
		log.Println("Cannot commit transaction with reports queue")
		done <- true
	}
	log.Println("Reports queue saved. Overall:", total, ". New:", count)
	done <- true
}
