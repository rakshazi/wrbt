# 1.0.1

* Restrict import/export only for supported by WarcraftLogs and Raider.IO raids (Legion and up)
* Added changelog

# 1.0.0 Gehennas

**Linux amd64**: [wrbt](/uploads/9cbb661e8c8d236869c9a21177b22437/wrbt) (sha256: c8497f4383764deeef0507be29363f7ce6b34da2990588ff83a7328d923b16dc)

Initial release

1. Grabs guilds from [Raider.IO](https://raider.io) raid progression API _Special thanks to Ludicrous Speed! Guys, you did awesome service!_
2. Grabs reports list for each guild from [Warcraft Logs](https://www.warcraftlogs.com/) API _Special thanks to Kihra! Without you a lot of things in WoW would be much worse!_
3. Parses each report from list (Warcraft Logs API) and gets boss kills/wipes (global counters for each boss/difficulty)
4. Grabs boss info (npc id, name, raid name) from Warcraft Logs API.
5. Export merged data to json

PS: [Gehennas](https://wowhead.com/npc=12259/) is the third boss of Molten Core.
