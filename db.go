package main

import (
	"log"

	bolt "go.etcd.io/bbolt"
)

// Setup database
func setupDb() {
	var err error = nil
	db, err = bolt.Open(dbpath, 0600, nil)
	if err != nil {
		log.Fatal("Cannot open database ", err)
	}
	// Start new transaction to create buckets
	tx, err := db.Begin(true)
	if err != nil {
		log.Fatal("Cannot start transaction ", err)
	}
	defer tx.Rollback()
	for _, bucket := range []string{"guilds", "reports", "reportsQueue", "bosses", "metadata"} {
		_, err = tx.CreateBucketIfNotExists([]byte(bucket))
		if err != nil {
			log.Fatal("Cannot create bucket ", bucket, err)
		}
	}
	err = tx.Commit()
	if err != nil {
		log.Fatal("Cannot commit transaction ", err)
	}
	log.Println("Initialization done")
}
