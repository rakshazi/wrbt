package main

import (
	"encoding/json"
	"log"
	"strconv"

	api "gitlab.com/rakshazi/warcraftlogs-api"
	rio "gitlab.com/rakshazi/wrbt/raiderio"
	wl "gitlab.com/rakshazi/wrbt/warcraftlogs"
	bolt "go.etcd.io/bbolt"
)

// Boss metadata
type Meta struct {
	Id       int
	Name     string
	ZoneName string
}

// List of suported raids
var raids = append(rio.Raids, []string{
	"Emerald Nightmare",
}...)

// Convert int to byte array
func int2byte(in int) []byte {
	return []byte(strconv.Itoa(in))
}

// Check if raid is supported
func isRaidSupported(name string) bool {
	for _, raid := range raids {
		if name == raid {
			return true
		}
	}
	return false
}

// Collect metadata from WarcraftLogs API /zones
func runMetadata() {
	var meta = make(chan *api.Zone)
	var done = make(chan bool)
	go saveMetadata(meta, done)
	wl.GetZones(workers, apikey, meta)
}

func saveMetadata(meta chan *api.Zone, done chan bool) {
	var count int //New items
	for zone := range meta {
		if isRaidSupported(zone.Name) == false {
			continue
		}
		tx, err := db.Begin(true)
		if err != nil {
			log.Println("Cannot start transaction to save metadata")
			done <- true
		}
		defer tx.Rollback()
		bucket := tx.Bucket([]byte("metadata"))
		for _, boss := range zone.Encounters {
			if boss.NpcId == 0 {
				// Not a raid boss, ignore it
				continue
			}
			item := bucket.Get(int2byte(boss.Id))
			if item != nil {
				//We already have it
				continue
			}
			metadata := &Meta{
				Id:       boss.NpcId,
				Name:     boss.Name,
				ZoneName: zone.Name,
			}
			value, err := json.Marshal(metadata)
			if err != nil {
				log.Println("Cannot marshal metadata object into bytes", err)
				continue
			}
			if err := bucket.Put(int2byte(boss.Id), value); err != nil {
				log.Println("Cannot save metadata to db", boss.Id, boss.Name, err)
				continue
			}
			count++
		}
		if err := tx.Commit(); err != nil {
			log.Println("Cannot commit transaction with metadata")
			done <- true
		}
	}
	var total int //Total items in db
	db.View(func(tx *bolt.Tx) error {
		tx.Bucket([]byte("metadata")).ForEach(func(k, v []byte) error {
			total++
			return nil
		})
		return nil
	})
	log.Println("Metadata saved. Overall:", total, ". New:", count)
	done <- true
}
