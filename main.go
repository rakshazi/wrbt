package main

import (
	"flag"
	"log"

	bolt "go.etcd.io/bbolt"
)

// Path to BBoltDB file
var dbpath string

// Path to export json file
var exportpath string

// WarcraftLogs API key
var apikey string

// Partial run wrbt
var part string

var db *bolt.DB
var workers int

//Init flags
func init() {
	flag.StringVar(&part, "only", "all", "Run only part of functionality, allowed: guilds, reports, metadata, calculate, export, all")
	flag.StringVar(&apikey, "apikey", "", "WarcraftLogs.com API Key")
	flag.StringVar(&dbpath, "db", "bolt.db", "Path to Bolt DB")
	flag.StringVar(&exportpath, "export", "export.json", "Path to json export file")
	flag.IntVar(&workers, "workers", 5, "Count of workers to spawn")
}

// After Init, setup db

// go Go GO
func main() {
	flag.Parse()
	if apikey == "" {
		log.Fatal("Wrong arguments specified. Check program help")
	}
	setupDb()
	defer db.Close()
	switch part {
	case "guilds":
		runGuilds()
	case "reports":
		runReports()
	case "metadata":
		runMetadata()
	case "calculate":
		runCalculate()
	case "export":
		runExport()
	default:
		runGuilds()
		runReports()
		runMetadata()
		runCalculate()
		runExport()
	}
}
