package main

import (
	"encoding/json"
	"fmt"
	"log"

	bolt "go.etcd.io/bbolt"

	wl "gitlab.com/rakshazi/wrbt/warcraftlogs"
)

type Boss struct {
	Id         int
	Difficulty int
	Kills      int
	Wipes      int
}

// Parse reports queue, get fights
func runCalculate() {
	var fights = make(chan *wl.Report)
	var done = make(chan bool)
	go calculate(fights, done)
	queue := getReportsQueue()
	wl.GetFights(workers, apikey, queue, fights)
}

// Get reports from db queue
func getReportsQueue() []string {
	var queue []string
	db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte("reportsQueue"))

		bucket.ForEach(func(k, v []byte) error {
			queue = append(queue, string(k))
			return nil
		})
		return nil
	})

	return queue
}

// Create or update boss info in db
func updateBoss(tx *bolt.Tx, bossId int, difficulty int, kill bool) {
	id := fmt.Sprintf("%d-%d", bossId, difficulty)
	item := tx.Bucket([]byte("bosses")).Get([]byte(id))
	var boss *Boss
	if item == nil { //No info about that boss
		boss = &Boss{
			Id:         bossId,
			Difficulty: difficulty,
			Kills:      0,
			Wipes:      0,
		}
	} else {
		boss = &Boss{}
		err := json.Unmarshal(item, boss)
		if err != nil {
			log.Println("Cannot unmarshal boss data to struct", err)
		}
	}
	if kill {
		boss.Kills++
	} else {
		boss.Wipes++
	}

	value, err := json.Marshal(boss)
	if err != nil {
		log.Println("Cannot marshal boss object into bytes", err)
		return
	}
	if err := tx.Bucket([]byte("bosses")).Put([]byte(id), value); err != nil {
		log.Println("Cannot save boss", id, err)
	}
}

// Calculate reports data
func calculate(reports chan *wl.Report, done chan bool) {
	var count int //New reports count
	for report := range reports {
		// Commit every report, because warcraft logs api is too slow
		tx, err := db.Begin(true)
		if err != nil {
			log.Println("Cannot start transaction to save reports data")
			done <- true
		}
		defer tx.Rollback()
		bucket := tx.Bucket([]byte("reports"))
		queueBucket := tx.Bucket([]byte("reportsQueue"))

		log.Println("Processing report", report.Id)
		item := bucket.Get([]byte(report.Id))
		if item != nil { // If report already parsed, remove it from queue
			queueBucket.Delete([]byte(report.Id))
			continue
		}
		for _, fight := range report.Fights {
			supported := tx.Bucket([]byte("metadatata")).Get(int2byte(fight.Boss)) //Check if we have metadata for that boss
			if fight.Boss == 0 || supported == nil || fight.Difficulty == 1 || fight.Difficulty == 10 {
				// Thrash fight or LFR or dungeon, ignore it.
				continue
			}
			updateBoss(tx, fight.Boss, fight.Difficulty, fight.Kill)
		}
		if err := bucket.Put([]byte(report.Id), nil); err != nil {
			log.Println("Cannot save report to reports bucket", report.Id, err)
		}
		queueBucket.Delete([]byte(report.Id))
		if err := tx.Commit(); err != nil {
			log.Println("Cannot commit transaction with parsed reports")
			done <- true
		}
		count++
	}
	log.Println("Reports parsed. New:", count)
	done <- true
}
