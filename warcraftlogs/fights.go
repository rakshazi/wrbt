package warcraftlogs

import (
	"log"
	"sync"

	wlapi "gitlab.com/rakshazi/warcraftlogs-api"
)

// Report data with fights
type Report struct {
	Id     string
	Fights []*wlapi.Pull
}

var reportIds = make(chan string)
var fights chan *Report

// Get report with fights by slice of report ids
func GetFights(workers int, apikey string, slice []string, fightsChan chan *Report) {
	log.Println("Start collecting fights...")
	if len(slice) > 0 {
		fights = fightsChan
		api = wlapi.New(apikey)
		go setReportIds(slice)
		runFightWorkersPool(workers)
	}
	log.Println("done")
}

// Set report ids to queue
func setReportIds(slice []string) {
	for _, id := range slice {
		reportIds <- id
	}
	close(reportIds)
}

// Spawns workers
func runFightWorkersPool(count int) {
	var wg sync.WaitGroup
	for i := 0; i < count; i++ {
		wg.Add(1)
		go fightWorker(&wg)
	}
	wg.Wait()
	close(fights)
}

// Worker will load report fights from WarcraftLogs API
func fightWorker(wg *sync.WaitGroup) {
	defer wg.Done()
	for id := range reportIds {
		list, err := api.Fights(id)
		if err != nil && err.Error() != "Not found on Warcraft Logs" {
			log.Println("Report", id, "failed:", err)
			continue
		}
		fight := &Report{
			Id:     id,
			Fights: list.Fights,
		}
		fights <- fight
	}
}
