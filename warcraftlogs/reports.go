package warcraftlogs

import (
	"log"
	"sync"

	wlapi "gitlab.com/rakshazi/warcraftlogs-api"
	rio "gitlab.com/rakshazi/wrbt/raiderio"
)

var guilds = make(chan *rio.Guild)
var reports chan string

// Get List of report ids for selected guilds
func GetReports(workers int, apikey string, slice []*rio.Guild, reportsChan chan string) {
	log.Println("Start collecting reports...")
	if len(slice) > 0 {
		reports = reportsChan
		api = wlapi.New(apikey)
		go setGuilds(slice)
		runGuildWorkersPool(workers)
	}
	log.Println("done")
}

// Set guilds to queue
func setGuilds(slice []*rio.Guild) {
	for _, guild := range slice {
		guilds <- guild
	}
	close(guilds)
}

// Spawns workers
func runGuildWorkersPool(count int) {
	var wg sync.WaitGroup
	for i := 0; i < count; i++ {
		wg.Add(1)
		go guildWorker(&wg)
	}
	wg.Wait()
	close(reports)
}

// Worker will load reports from WarcraftLogs API
func guildWorker(wg *sync.WaitGroup) {
	defer wg.Done()
	for guild := range guilds {
		list, err := api.ReportsForGuild(guild.Name, guild.Realm, guild.Region)
		if err != nil {
			log.Println("Guild", guild.Name, "@", guild.Realm, guild.Region, "failed:", err)
			continue
		}
		for _, report := range list {
			reports <- report.Id
		}
	}
}
