package warcraftlogs

import (
	"log"
	"sync"

	wlapi "gitlab.com/rakshazi/warcraftlogs-api"
)

var zones chan *wlapi.Zone

// Get List of zones from Warcraft Logs API
func GetZones(workers int, apikey string, zonesChan chan *wlapi.Zone) {
	log.Println("Start collecting zones...")
	zones = zonesChan
	api = wlapi.New(apikey)
	runZoneWorkersPool(workers)

	log.Println("done")
}

// Spawns workers
func runZoneWorkersPool(count int) {
	var wg sync.WaitGroup
	for i := 0; i < count; i++ {
		wg.Add(1)
		go zoneWorker(&wg)
	}
	wg.Wait()
	close(zones)
}

// Worker will load reports from WarcraftLogs API
func zoneWorker(wg *sync.WaitGroup) {
	defer wg.Done()
	list, err := api.Zones()
	if err != nil {
		log.Println("Zones failed:", err)
		wg.Done()
	}
	for _, zone := range list {
		zones <- zone
	}
}
