package raiderio

// List of raids
var Raids = []string{
	"The Eternal Palace",
	"Battle of Dazar'alor",
	"Uldir",
	"Crucible of Storms",
	"Tomb of Sargeras",
	"The Emerald Nightmare",
	"The Nighthold",
	"Antorus, The Burning Throne",
	"Trial of Valor",
}

// Raider.IO regions
var Regions = []string{
	"world",
	"us",
	"eu",
}

// Raider.IO raid difficulties
var Difficulties = []string{
	"normal",
	"heroic",
	"mythic",
}

// Russian realm names translation, required by WarcraftLogs API (v_v)
var RuRealmTranslation = map[string]string{
	"ashenvale":     "ясеневыи-лес",
	"azuregos":      "азурегос",
	"blackscar":     "черныи-шрам",
	"booty-bay":     "пиратская-бухта",
	"borean-tundra": "бореиская-тундра",
	"deathguard":    "страж-смерти",
	"deathweaver":   "ткач-смерти",
	"deepholm":      "подземье",
	"eversong":      "вечная-песня",
	"fordragon":     "дракономор",
	"galakrond":     "галакронд",
	"goldrinn":      "голдринн",
	"gordunni":      "гордунни",
	"greymane":      "седогрив",
	"grom":          "гром",
	"howling-fjord": "ревущии-фьорд",
	"lich-king":     "король-лич",
	"razuvious":     "разувии",
	"soulflayer":    "свежеватель-душ",
	"thermaplugg":   "термоштепсель",
}
