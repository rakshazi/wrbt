package raiderio

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

var client *http.Client = &http.Client{}

func GetGuilds(raid, difficulty, region string) ([]*Guild, error) {
	var progression = &ViewRaidingProgressionResponse{}
	urlString := "https://raider.io/api/v1/raiding/progression?raid=" + url.QueryEscape(raid) + "&difficulty=" + difficulty + "&region=" + region
	req, err := http.NewRequest("GET", urlString, nil)
	if nil != err {
		return nil, err
	}
	req.Header.Set("User-Agent", "curl/7.65.3")
	req.Header.Set("Host", "raider.io")
	req.Header.Set("Accept", "*/*")

	httpResponse, err := client.Do(req)
	if nil != err && httpResponse.StatusCode != 429 {
		return nil, err
	}
	if httpResponse.StatusCode == 429 {
		log.Println("API Rate Limit reached. Sleep 1 minute")
		time.Sleep(1 * time.Minute)
		return GetGuilds(raid, difficulty, region)
	}
	defer httpResponse.Body.Close()
	data, err := ioutil.ReadAll(httpResponse.Body)
	if nil != err {
		return nil, err
	}
	err = json.Unmarshal(data, &progression)
	if nil != err {
		return nil, err
	}
	guilds := []*Guild{}
	for _, progressionItem := range progression.Progression {
		for _, progressionItemData := range progressionItem.Guilds {
			switch progressionItemData.Guild.Region.Slug {
			case "kr", "tw", "cn":
				// Unsupported region in Warcraft Logs
				continue
			}
			// Workaround for weird WarcraftLogs transliteration
			if progressionItemData.Guild.Region.ShortName == "RU" {
				progressionItemData.Guild.Realm.Slug = RuRealmTranslation[progressionItemData.Guild.Realm.Slug]
			}
			guild := &Guild{
				Name:   progressionItemData.Guild.Name,
				Realm:  progressionItemData.Guild.Realm.Slug,
				Region: progressionItemData.Guild.Region.Slug,
			}
			guilds = append(guilds, guild)
		}
	}
	return guilds, nil
}
